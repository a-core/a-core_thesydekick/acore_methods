"""
acore_methods
=============

Common utilities for working with sdk entities.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))
from thesdk import *

class acore_methods:
    def __init__(self):
        ...

    @staticmethod
    def init_props_from_kwargs(entity, **kwargs):
        """
        Initialize properties from keyword arguments.

        Properties are stored in class object instead of class instance.
        Furthermore, properties are not inherited so we also need to traverse
        the hierarchy of supertypes to properly initialize any properties defined
        by supertypes.

        see: https://docs.python.org/3/reference/datamodel.html
        """

        def traverse_supertypes(cls, attrs):
            """traverse """
            supertypes = cls.__bases__
            for supertype in supertypes:
                traverse_supertypes(supertype, attrs)
            attrs.update(cls.__dict__)
        
        attrs = {}
        traverse_supertypes(type(entity), attrs)

        # iterate over kwargs and update any matching entity property using its setter
        entity.print_log(type='I', msg='Initializing properties from kwargs:')
        for (key, value) in kwargs.items():
            if key in attrs.keys() and isinstance(attrs[key], property):
                entity.print_log(type='I', msg='    {:20s} = {}'.format(key, value))
                setattr(entity, key, value)
