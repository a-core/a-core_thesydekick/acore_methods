import functools

def profile(func):
    """Profile execution time of the decorated function"""
    @functools.wraps(func)
    def profile_wrapper(*args, **kwargs):
        import cProfile, pstats
        profiler = cProfile.Profile()
        profiler.enable()
        retval = func(*args, **kwargs)
        profiler.disable()
        stats = pstats.Stats(profiler).sort_stats('cumtime')
        stats.print_stats()
        return retval
    return profile_wrapper
