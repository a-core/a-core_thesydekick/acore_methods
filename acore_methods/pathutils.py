import os

def barename(abspath):
    """
    Extract the name of a file from an absolute path without its file extension.

    Example
    -------

    ::

        >>> from acore_methods.pathutils import barename
        >>> barename('/some/long/path/to/filename.ext')
            'filename'
    """
    return os.path.splitext(os.path.basename(abspath))[0]