.. toctree::
   :maxdepth: 3
   :caption: Contents:


API Documentation
=================

.. automodule:: acore_methods
   :members:
   :special-members: __init__
   :undoc-members:

.. automodule:: acore_methods.decorators
   :members:
   :undoc-members:

.. automodule:: acore_methods.pathutils
   :members:
   :undoc-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

